# dotfiles
Scripts, configs, and dotfiles needed for my desktop and laptop setups.

This is designed to be used with [GNU Stow](https://www.gnu.org/software/stow/). Specifically,
entering the `stow` directory and running `stow -t ~ *` will link all dotfiles.


