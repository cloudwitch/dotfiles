#!/bin/sh
## Add Global Kernel tweaking.

touch /etc/sysctl.conf
mkdir -p /etc/sysctl.d/

# Enable namespaces in kernel.

echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/00-local-userns.conf
sysctl --load
